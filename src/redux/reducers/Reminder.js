import { ADD_REMINDER, UPDATE_REMINDER } from '../../@jumbo/constants/ActionTypes';

const INIT_STATE = {
  reminder: [],
};

export default (state = INIT_STATE, action) => {
  switch (action.type) {
    case ADD_REMINDER: {
      let tempReminders = state.reminder;
      tempReminders.push(action.payload); console.log('REMINDERSS reducer',action.payload, tempReminders)
      return { ...state, reminders: tempReminders };
    }

    case UPDATE_REMINDER: {
      const { id } = action.payload;
    
      let tempReminders = state.reminders;
      const index = tempReminders.findIndex(x => x.id === id);
      tempReminders[index] = action.payload;

      return { ...state, reminders: tempReminders };
    }

    default:
      return state;
  }
};

import { ADD_REMINDER, UPDATE_REMINDER } from '../../@jumbo/constants/ActionTypes';

export const addReminder = (reminder) => { 
  return dispatch => {
    dispatch({
      type: ADD_REMINDER,
      payload: reminder
    });
  };
};

export const updateReminder = (reminder) => { 
  return dispatch => {
    dispatch({
      type: UPDATE_REMINDER,
      payload: reminder
    });
  };
};


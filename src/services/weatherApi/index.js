import React from 'react';
import axios from '../configApiWeather';

export const weather = {
  getWeather: async (city) => {

    const apiKey = "e32dcd433918ec89c8a5576d65da82c5";
    const response = await axios.get(`?appid=${apiKey}`, {params: {
      q: city,
      units:'metric',
      cnt: 1,
      dt: '1075896000',

    }})
      .then(({ data }) => {
        if (data) {
          console.log('DATA RESPONSED WEATHER => ', data)
          const { list } = data;
            const { main, weather } = list[0];
            return { main, weather };
        } else {
            console.log('ERRORS ',data)
            return data;
        }
      })
      .catch(function (error) { 
        return error;
      });

    return response;
  },
};
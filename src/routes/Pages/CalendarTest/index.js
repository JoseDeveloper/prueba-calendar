import React from 'react';
import GridContainer from '../../../@jumbo/components/GridContainer';
import PageContainer from '../../../@jumbo/components/PageComponents/layouts/PageContainer';
import Box from '@material-ui/core/Box';
import IntlMessages from '../../../@jumbo/utils/IntlMessages';
import Grid from '@material-ui/core/Grid';
import VisibilityIcon from '@material-ui/icons/Visibility';
import { makeStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import Button from "@material-ui/core/Button";
import events from './events';
import Calendar from './customComponents/calendar';

const breadcrumbs = [
  { label: 'Home', link: '/' },
  { label: 'Calendar', isActive: true },
];

const useStyles = makeStyles({
  table: {
    minWidth: 650
  }
});

const CalendarTest = () => {
  const classes = useStyles();

  const handleSelectEvent = (row) => {
    // e.preventDefault();
    console.log("The Values that you wish to edit ", row);
  };

  const handleSelectSlot = (e, row) => {
    console.log("SLOT ", row);
  }


  return (
    <PageContainer heading={<IntlMessages id="pages.calendarTest" />} breadcrumbs={breadcrumbs}>
      <GridContainer>
        <Grid item xs={12}>
          <Box>
            <IntlMessages id="pages.calendarTest" />
          </Box>
        </Grid>
        <TableContainer component={Paper}>
          <Calendar />
        </TableContainer>
      </GridContainer>
    </PageContainer>
  );
};

export default CalendarTest;

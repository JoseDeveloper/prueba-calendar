import React from 'react';
import moment from 'moment';
import Day from './day';

class Week extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
        };

    }

    // componentWillReceiveProps(nextProps) {
       
    //     if (nextProps.reminders !== this.props.reminders) { console.log('REMINDERSS IN WEEK',nextProps.reminders)
    //         this.setState({ filerReminders: nextProps.reminder });
    //     }
    // }

    render() {
        let days = [];
        let {
            date,
        } = this.props;

        const {
            month,
            selected,
            select
        } = this.props;

        for (var i = 0; i < 7; i++) {
            let day = {
                name: date.format("dd").substring(0, 1),
                number: date.date(),
                isCurrentMonth: date.month() === month.month(),
                isToday: date.isSame(new Date(), "day"),
                date: date
            };

            days.push(
                <Day day={day}
                    selected={selected}
                    select={select}
                    month={month}
                />
            );

            date = date.clone();
            date.add(1, "day");
        }

        return (
            <div className="row week" key={days[0]}>
                {days}
            </div>
        );
    }

}

export default Week;
import React from 'react';
import { connect } from 'react-redux';
import moment from 'moment';
import { makeStyles } from '@material-ui/core/styles';
import Popover from '@material-ui/core/Popover';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import FormDialog from './modal';

class Day extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            anchorEl: null,
            openDialog: false,
            reminders: [],
            filterReminders: [],
            dialogData: {}
        };

    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.reminders.reminders !== this.props.reminders.reminders) { 
            this.setState({ reminders: nextProps.reminders.reminders }); 
        }
    }

    setAnchorEl = (current) => {
        this.setState({ anchorEl: current });
    }

    handleClick = (event) => {
        this.setAnchorEl(event.currentTarget);
    };

    handleClose = () => {
        this.setAnchorEl(null);
    };

    seePopUpItems(e, day, reminderList) {
        e.preventDefault();
        if (reminderList) {
            console.log('POP UPSSS ', day)
        }

    }

    clickDay(e, day, reminderList) {
        const { select, reminders } = this.props;
        console.log('REMINDER LIST day ', day, reminderList)
        

        if(!reminderList){
            select(day);
            this.setState({openDialog: true });
        }else{
            
            const { reminders } = this.state; 
            const dayMoment = moment(day.date);
            const filterReminders = reminders.filter(item => { 
                if( moment(item.date.date).isSame(dayMoment,'day') ){
                    return item;
                }
            });

            console.log('ITEM FILTER > ',filterReminders)

            this.setState({anchorEl: e.currentTarget, openDialog: false, filterReminders });
        }
    }

    closeDialog = () => {
        this.setState({openDialog: false });
    }

    getReminderItem = (e, item) => {
        e.preventDefault();
        console.log('ITEM get reminder ',item)
        this.setState({openDialog: true, dialogData: item });
    }

    render() {
        const {
            day,
            day: {
                date,
                isCurrentMonth,
                isToday,
                number
            },
            select,
            selected,
            month,
        } = this.props;

        const open = Boolean(this.state.anchorEl);
        const id = open ? 'simple-popover' : undefined;
        const { filterReminders, dialogData } = this.state;

        return (
            <>
                <span
                    key={date.toString()}
                    className={"day" + (isToday ? " today" : "") + (isCurrentMonth ? "" : " different-month") + (date.isSame(selected) ? " selected" : "")}
                >
                    <a href="#" onClick={(e) => this.clickDay(e, day, true)} className="aDay"> {number} </a>

                    <a href="#" className="reminder-down" onClick={(e) => this.clickDay(e, day, false)}> Add </a>
                </span>

                <Popover
                    id={id}
                    open={open}
                    anchorEl={this.state.anchorEl}
                    onClose={this.handleClose}
                    anchorOrigin={{
                        vertical: 'top',
                        horizontal: 'right',
                    }}
                    transformOrigin={{
                        vertical: 'top',
                        horizontal: 'center',
                    }}
                >
                    { filterReminders &&
                        filterReminders.map((item, i) => { console.log('ITEMsss =>',item)
                            return (
                                <>
                                    <Button variant="contained" color="primary" onClick={(e) => this.getReminderItem(e, item)}>
                                        {item.text}
                                    </Button>
                                    <br></br>
                                </>
                            )
                        })
                    }

                </Popover>

                <FormDialog openDialog={this.state.openDialog} day={day} dialogData={dialogData} month={month} closeDialog={this.closeDialog} />
            </>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        reminders: state.reminder
    }
}

export default connect(
    mapStateToProps
)(Day);

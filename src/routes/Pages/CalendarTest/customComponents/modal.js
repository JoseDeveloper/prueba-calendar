import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { makeStyles } from "@material-ui/core/styles";
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import ColorLensIcon from '@material-ui/icons/ColorLens';

import { weather } from '../../../../../src/services/weatherApi';

import { addReminder, updateReminder } from '../../../../redux/actions/Reminder';

const colors = [{ 'name': 'Green', 'code': '#18D131' }, { 'name': 'Blue', 'code': '#1834D1' }, { 'name': 'Red', 'code': '#D11818' }]
const cities = [{ 'name': 'Caracas' }, { 'name': 'Madrid' }, { 'name': 'Barcelona' }];

const useStyles = makeStyles({
    width: {
        width: '50%',
        padding: '10px'
    },
    padding: {
        padding: '10px'
    }
});


export default function FormDialog({ openDialog, day, dialogData, month, closeDialog }) {
    const classes = useStyles();

    const { authUser } = useSelector(({ auth }) => auth);
    const { reminder } = useSelector(({ reminder }) => reminder);
    const dispatch = useDispatch();

    const [open, setOpen] = React.useState(false);
    const [dayDate, setDay] = React.useState({});
    // const [data, setData] = React.useState(null);
    const [city, setCity] = React.useState(null);
    const [color, setColor] = React.useState(null);
    const [text, setText] = React.useState(null);
    const [id, setId] = React.useState(null);
    const [tempWeather, setTemp] = React.useState(null);
    const [descriptionWeather, setDesc] = React.useState(null);

    useEffect(() => {
        console.log('OPEN MODAL DIALOG ', day, dialogData)
        setOpen(openDialog);
        if (day) {
            setDay(day);
        }

        if (dialogData) {
            const { id, city, color, text } = dialogData;
            setCity(city);
            getForecast(city);
            setColor(color);
            setText(text);
            setId(id);
        }

    }, [openDialog, dialogData]);

    const handleClose = () => {
        setOpen(false);
        closeDialog();
    };

    const handleChangeColor = (e) => {
        setColor(e.target.value)
    }

    const handleChangeCity = async (e) => {
        setCity(e.target.value)
        getForecast(e.target.value);
    }

    const getForecast = async (value) => {
        const response = await weather.getWeather(value);
        console.log('RESPONSE weatherss', response)
        const { main, weather: weatherData } = response;
        if (main) {
            setTemp(main.temp);
            setDesc(weatherData[0].description);
        }

    }

    const handleChangeText = (e) => {
        setText(e.target.value);
    }

    const saveReminder = () => {
        console.log('SAVE REMINDER ', month, dayDate, text, color, city)
        console.log('REMINDERS from select ', reminder)
        const id = reminder.length + 1;
        const objectReminder = { id, month, date: dayDate, text, color, city };
        dispatch(addReminder(objectReminder));

    }

    const handleUpdateReminder = (e) => {
        e.preventDefault();
        console.log('Update REMINDER ', id, month, dayDate, text, color, city)
        console.log('REMINDERS from select ', reminder)
        const objectReminder = { id, month, date: dayDate, text, color, city };
        dispatch(updateReminder(objectReminder));

    }

    return (
        <div>
            <Dialog
                open={open}
                onClose={handleClose}
                fullWidth={true}
                maxWidth={'sm'}
                aria-labelledby="form-dialog-title"
            >
                <DialogTitle id="form-dialog-title">Create reminder</DialogTitle>
                <DialogContent>
                    <DialogContentText>
                        Dialog context
                    </DialogContentText>
                    <TextField className={classes.width}
                        value={text}
                        autoFocus
                        margin="dense"
                        label="Text"
                        type="text"
                        className={classes.width}
                        fullWidth
                        onChange={handleChangeText}
                    />

                    <FormControl className={classes.width} >
                        <InputLabel id="demo-simple-select-label">Select color</InputLabel>
                        <Select
                            labelId="demo-simple-select-label"
                            id="demo-simple-select"
                            value={color}
                            onChange={handleChangeColor}
                        >
                            {(colors && colors.length > 0) &&
                                colors.map((item, i) => {
                                    return <MenuItem value={item.code}> {item.name} <ColorLensIcon styke={{ color: item.code }} /></MenuItem>
                                })
                            }
                        </Select>
                    </FormControl>

                    <FormControl className={classes.width} >
                        <InputLabel id="demo-simple-select-label">Select City</InputLabel>
                        <Select
                            // disabled={}
                            labelId="demo-simple-select-label"
                            id="demo-simple-select"
                            value={city}
                            onChange={handleChangeCity}
                        >
                            {(cities && cities.length > 0) &&
                                cities.map((item, i) => {
                                    return <MenuItem value={item.name}> {item.name} </MenuItem>
                                })
                            }
                        </Select>
                    </FormControl>

                    <FormControl className={classes.width} >
                        {descriptionWeather &&
                            <>
                                <InputLabel id="demo-simple-select-label">Weather forecast values: </InputLabel><br></br><br></br>
                                <span> Temperature: {tempWeather} </span><br></br>
                                <span> Sky: {descriptionWeather} </span><br></br>
                            </>
                        }

                    </FormControl>


                </DialogContent>
                <DialogActions>
                    <Button onClick={handleClose} color="primary">
                        Cancel
                    </Button>
                    {(id) ? <Button onClick={(e) => handleUpdateReminder(e)} color="primary">Update</Button>
                        :
                        <Button onClick={saveReminder} color="primary">Save</Button>
                    }
                </DialogActions>
            </Dialog>
        </div>
    );
}
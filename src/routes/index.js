import React from 'react';
import { Redirect, Route, Switch } from 'react-router';
import { useSelector } from 'react-redux';
import { useLocation } from 'react-router-dom';
import CalendarTest from './Pages/CalendarTest'; 
import Error404 from './Pages/404';
import Login from './Auth/Login';
import Register from './Auth/Register';
import ForgotPasswordPage from './Auth/ForgotPassword';

const RestrictedRoute = ({ component: Component, ...rest }) => {
  const { authUser } = useSelector(({ auth }) => auth);

  return (
    <Route
      {...rest}
      render={props =>
        authUser ? (
          <Component {...props} />
        ) : (
            <Redirect
              to={{
                pathname: '/signin',
                state: { from: props.location },
              }}
            />
          )
      }
    />
  );
};

const Routes = () => {
  const { authUser } = useSelector(({ auth }) => auth);
  const location = useLocation();

  if (location.pathname === '' || location.pathname === '/') {
    if (authUser && authUser.role_id === 2) {
      return <Redirect to={'/calendar-test'} />;
    } else {
      return <Redirect to={'/calendar-test'} />;
    }

  } else if (authUser && authUser && location.pathname === '/signin') {
    if (authUser.role_id === 2) {
      return <Redirect to={'/calendar-test'} />;
    } else {
      return <Redirect to={'/calendar-test'} />;
    }
  }

  return (
    <React.Fragment>
      <Switch>
        <RestrictedRoute path="/calendar-test" component={CalendarTest} />
        <Route path="/signin" component={Login} />
        <Route path="/signup" component={Register} />
        <Route path="/forgot-password" component={ForgotPasswordPage} />
        <Route component={Error404} />
      </Switch>
    </React.Fragment>
  );
};

export default Routes;
